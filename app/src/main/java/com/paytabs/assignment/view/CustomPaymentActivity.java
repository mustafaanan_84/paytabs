package com.paytabs.assignment.view;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.devmarvel.creditcardentry.library.CreditCardForm;
import com.paytabs.assignment.R;
import com.paytabs.assignment.utility.Constants;
import com.paytabs.paytabs_sdk.payment.ui.activities.PayTabActivity;
import com.paytabs.paytabs_sdk.payment.ui.fragments.PaymentFragment;

public class CustomPaymentActivity extends PayTabActivity {

    private String passedCardName;
    private String passedCardNumber;
    private String passedCardMonth;
    private String passedCardYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle extrasBundle = getIntent().getExtras();
            passedCardName = extrasBundle.getString(Constants.SCAN_CARD_NAME, "");
            passedCardNumber = extrasBundle.getString(Constants.SCAN_CARD_NUMBER, "");
            passedCardMonth = extrasBundle.getString(Constants.SCAN_CARD_MONTH, "");
            passedCardYear = extrasBundle.getString(Constants.SCAN_CARD_YEAR, "");
        }

    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PaymentFragment) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(passedCardName)) {
                        ((EditText) findViewById(R.id.cardholder_name)).setText(passedCardName);
                    }
                    if (!TextUtils.isEmpty(passedCardNumber)) {
                        ((CreditCardForm) findViewById(R.id.card_number)).setCardNumber(passedCardNumber, true);
                    }
                    if (!TextUtils.isEmpty(passedCardMonth)) {
                        Spinner monthSpinner = ((Spinner) findViewById(R.id.exp_month));
                        for (int monthCount = 0; monthCount < monthSpinner.getAdapter().getCount(); monthCount++) {
                            if (monthSpinner.getAdapter().getItem(monthCount).toString().equals(passedCardMonth)) {
                                monthSpinner.setSelection(monthCount);
                                break;
                            }
                        }
                    }
                    if (!TextUtils.isEmpty(passedCardYear)) {
                        Spinner yearSpinner = ((Spinner) findViewById(R.id.exp_year));
                        for (int yearCount = 0; yearCount < yearSpinner.getAdapter().getCount(); yearCount++) {
                            if (yearSpinner.getAdapter().getItem(yearCount).toString().equals(passedCardYear)) {
                                yearSpinner.setSelection(yearCount);
                                break;
                            }
                        }
                    }

                }
            }, 500);
        }
    }
}
