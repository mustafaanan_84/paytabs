package com.paytabs.assignment.view;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.paytabs.assignment.R;
import com.paytabs.paytabs_sdk.utils.PaymentParams;

public class PaymentResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_result);

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().size() > 0) {
            String responseCode = getIntent().getStringExtra(PaymentParams.RESPONSE_CODE);
            String transactionId = getIntent().getStringExtra(PaymentParams.TRANSACTION_ID);
            String message = getIntent().getStringExtra(PaymentParams.RESULT_MESSAGE);

            ((TextView) findViewById(R.id.tv_result_code)).setText(responseCode);
            ((TextView) findViewById(R.id.tv_transaction_id)).setText(transactionId);
            ((TextView) findViewById(R.id.tv_result_message)).setText(message);
        }

    }
}
