package com.paytabs.assignment.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.paytabs.assignment.R;
import com.paytabs.assignment.utility.Constants;
import com.paytabs.assignment.model.PaymentRequestModel;
import com.paytabs.paytabs_sdk.utils.PaymentParams;

import cards.pay.paycardsrecognizer.sdk.Card;
import cards.pay.paycardsrecognizer.sdk.ScanCardIntent;
import cards.pay.paycardsrecognizer.sdk.ui.ScanCardActivity;

public class FormActivity extends AppCompatActivity {

    private PaymentRequestModel paymentRequestModel;
    private String token;
    private String cmail;
    private String cpwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        initModel();
        registerListeners();
    }

    private void registerListeners() {
        findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), ScanCardActivity.class);
                startActivityForResult(in, 2000);
            }
        });

        findViewById(R.id.btn_pay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fillFormModel();
                goPayment();
            }
        });

        ((Switch) findViewById(R.id.switch_tokenization)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                token = null;
                cmail = null;
                cpwd = null;

            }
        });
    }

    private void initModel() {
        if (paymentRequestModel == null) {
            paymentRequestModel = new PaymentRequestModel();
        }
    }


    private void fillFormModel() {
        paymentRequestModel.setTitle(((EditText) findViewById(R.id.et_title)).getText().toString());
        paymentRequestModel.setOrderId(((EditText) findViewById(R.id.et_order_id)).getText().toString());
        paymentRequestModel.setProductName(((EditText) findViewById(R.id.et_product_name)).getText().toString());
        paymentRequestModel.setCurrency(((EditText) findViewById(R.id.et_currency)).getText().toString());
        if (TextUtils.isEmpty(((EditText) findViewById(R.id.et_amount)).getText().toString())) {
            paymentRequestModel.setAmount(0.0);
        } else {
            paymentRequestModel.setAmount(Double.parseDouble(((EditText) findViewById(R.id.et_amount)).getText().toString()));
        }
        paymentRequestModel.setPhone(((EditText) findViewById(R.id.et_phone)).getText().toString());
        paymentRequestModel.setEmail(((EditText) findViewById(R.id.et_email)).getText().toString());

        paymentRequestModel.setBillingAddress(((EditText) findViewById(R.id.et_billing_address)).getText().toString());
        paymentRequestModel.setBillingCity(((EditText) findViewById(R.id.et_billing_city)).getText().toString());
        paymentRequestModel.setBillingState(((EditText) findViewById(R.id.et_billing_state)).getText().toString());
        paymentRequestModel.setBillingCountry(((EditText) findViewById(R.id.et_billing_country)).getText().toString());
        paymentRequestModel.setBillingPostalCode(((EditText) findViewById(R.id.et_billing_postal)).getText().toString());

        paymentRequestModel.setShippingAddress(((EditText) findViewById(R.id.et_shipping_address)).getText().toString());
        paymentRequestModel.setShippingCity(((EditText) findViewById(R.id.et_shipping_city)).getText().toString());
        paymentRequestModel.setShippingState(((EditText) findViewById(R.id.et_shipping_state)).getText().toString());
        paymentRequestModel.setShippingCountry(((EditText) findViewById(R.id.et_shipping_country)).getText().toString());
        paymentRequestModel.setShippingPostalCode(((EditText) findViewById(R.id.et_shipping_postal)).getText().toString());
//        setTestData();
    }


//    private void setTestData() {
//        paymentRequestModel.setTitle("Title");
//        paymentRequestModel.setOrderId("Order1234");
//        paymentRequestModel.setProductName("ProduceName");
//        paymentRequestModel.setCurrency("EGP");
//        paymentRequestModel.setAmount(5.0);
//        paymentRequestModel.setPhone("01002026886");
//        paymentRequestModel.setEmail("mustafa.anan@gmail.com");
//
//        paymentRequestModel.setBillingAddress("Address");
//        paymentRequestModel.setBillingCity("Cairo");
//        paymentRequestModel.setBillingState("Cairo");
//        paymentRequestModel.setBillingCountry("EGY");
//        paymentRequestModel.setBillingPostalCode("12345");
//
//        paymentRequestModel.setShippingAddress("Address");
//        paymentRequestModel.setShippingCity("Cairo");
//        paymentRequestModel.setShippingState("Cairo");
//        paymentRequestModel.setShippingCountry("EGY");
//        paymentRequestModel.setShippingPostalCode("12345");
//    }


    private String validateForm() {
        String message = null;
        if (paymentRequestModel != null) {

            if (TextUtils.isEmpty(paymentRequestModel.getTitle())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.transaction_title).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getOrderId())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.order_id).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getProductName())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.product_name).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getCurrency())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.currency_code).toLowerCase();
            } else if (paymentRequestModel.getAmount() <= 0) {
                message = getString(R.string.error_empty) + " " + getString(R.string.amount).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getPhone())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.customer_phone).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getEmail())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.customer_email).toLowerCase();
            } else if (!Patterns.EMAIL_ADDRESS.matcher(paymentRequestModel.getEmail()).matches()) {
                message = getString(R.string.error_invalid) + " " + getString(R.string.customer_email).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getBillingAddress())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.billing).toLowerCase() + " " + getString(R.string.address).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getBillingCity())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.billing).toLowerCase() + " " + getString(R.string.city).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getBillingState())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.billing).toLowerCase() + " " + getString(R.string.state).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getBillingCountry())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.billing).toLowerCase() + " " + getString(R.string.country).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getShippingAddress())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.shipping).toLowerCase() + " " + getString(R.string.address).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getShippingCity())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.shipping).toLowerCase() + " " + getString(R.string.city).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getShippingState())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.shipping).toLowerCase() + " " + getString(R.string.state).toLowerCase();
            } else if (TextUtils.isEmpty(paymentRequestModel.getShippingCountry())) {
                message = getString(R.string.error_empty) + " " + getString(R.string.shipping).toLowerCase() + " " + getString(R.string.country).toLowerCase();
            }

        } else {
            message = getString(R.string.error_invalid_data);
        }
        return message;
    }

    private void goPayment() {
        String message = validateForm();
        if (message == null) {
            Intent paymentIntent = new Intent(getApplicationContext(), CustomPaymentActivity.class);
            paymentIntent.putExtra(PaymentParams.MERCHANT_EMAIL, Constants.MERCHANT_EMAIL);
            paymentIntent.putExtra(PaymentParams.SECRET_KEY, Constants.SECRET_KEY);
            paymentIntent.putExtra(PaymentParams.LANGUAGE, PaymentParams.ENGLISH);
            paymentIntent.putExtra(PaymentParams.TRANSACTION_TITLE, paymentRequestModel.getTitle());
            paymentIntent.putExtra(PaymentParams.CURRENCY_CODE, paymentRequestModel.getCurrency());
            paymentIntent.putExtra(PaymentParams.AMOUNT, paymentRequestModel.getAmount());

            paymentIntent.putExtra(PaymentParams.CUSTOMER_PHONE_NUMBER, paymentRequestModel.getPhone());
            paymentIntent.putExtra(PaymentParams.CUSTOMER_EMAIL, paymentRequestModel.getEmail());
            paymentIntent.putExtra(PaymentParams.ORDER_ID, paymentRequestModel.getOrderId());
            paymentIntent.putExtra(PaymentParams.PRODUCT_NAME, paymentRequestModel.getProductName());

            paymentIntent.putExtra(PaymentParams.ADDRESS_BILLING, paymentRequestModel.getBillingAddress());
            paymentIntent.putExtra(PaymentParams.CITY_BILLING, paymentRequestModel.getBillingCity());
            paymentIntent.putExtra(PaymentParams.STATE_BILLING, paymentRequestModel.getBillingState());
            paymentIntent.putExtra(PaymentParams.COUNTRY_BILLING, paymentRequestModel.getBillingCountry());
            if (TextUtils.isEmpty(paymentRequestModel.getBillingPostalCode())) {
                paymentIntent.putExtra(PaymentParams.POSTAL_CODE_BILLING, Constants.DEFAULT_POSTAL);
            } else {
                paymentIntent.putExtra(PaymentParams.POSTAL_CODE_BILLING, paymentRequestModel.getBillingPostalCode());
            }

            paymentIntent.putExtra(PaymentParams.ADDRESS_SHIPPING, paymentRequestModel.getShippingAddress());
            paymentIntent.putExtra(PaymentParams.CITY_SHIPPING, paymentRequestModel.getShippingCity());
            paymentIntent.putExtra(PaymentParams.STATE_SHIPPING, paymentRequestModel.getShippingState());
            paymentIntent.putExtra(PaymentParams.COUNTRY_SHIPPING, paymentRequestModel.getShippingCountry());
            if (TextUtils.isEmpty(paymentRequestModel.getShippingPostalCode())) {
                paymentIntent.putExtra(PaymentParams.POSTAL_CODE_BILLING, Constants.DEFAULT_POSTAL);
            } else {
                paymentIntent.putExtra(PaymentParams.POSTAL_CODE_BILLING, paymentRequestModel.getBillingPostalCode());
            }

            paymentIntent.putExtra(Constants.SCAN_CARD_NAME, paymentRequestModel.getCardName());
            paymentIntent.putExtra(Constants.SCAN_CARD_NUMBER, paymentRequestModel.getCardNumber());
            paymentIntent.putExtra(Constants.SCAN_CARD_MONTH, paymentRequestModel.getCardMonth());
            paymentIntent.putExtra(Constants.SCAN_CARD_YEAR, paymentRequestModel.getCardYear());

            paymentIntent.putExtra(PaymentParams.PAY_BUTTON_COLOR, "#0075c9");

            if (((Switch) findViewById(R.id.switch_tokenization)).isChecked()) {
                paymentIntent.putExtra(PaymentParams.IS_TOKENIZATION, true);
            } else {
                paymentIntent.putExtra(PaymentParams.IS_TOKENIZATION, false);
            }
            if (((Switch) findViewById(R.id.switch_pre_auth)).isChecked()) {
                paymentIntent.putExtra(PaymentParams.IS_PREAUTH, true);

                if (!TextUtils.isEmpty(token)) {
                    paymentIntent.putExtra(PaymentParams.TOKEN, token);
                }
                if (!TextUtils.isEmpty(cmail)) {
                    paymentIntent.putExtra(PaymentParams.CUSTOMER_EMAIL, cmail);
                }
                if (!TextUtils.isEmpty(cpwd)) {
                    paymentIntent.putExtra(PaymentParams.CUSTOMER_PASSWORD, cpwd);
                }

            } else {
                paymentIntent.putExtra(PaymentParams.IS_PREAUTH, false);
            }


            startActivityForResult(paymentIntent, PaymentParams.PAYMENT_REQUEST_CODE);
        } else {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PaymentParams.PAYMENT_REQUEST_CODE) {
            Log.e("Tag", data.getStringExtra(PaymentParams.RESPONSE_CODE));
            Log.e("Tag", data.getStringExtra(PaymentParams.RESULT_MESSAGE));
            Log.e("Tag", data.getStringExtra(PaymentParams.TRANSACTION_ID));

            if (data.hasExtra(PaymentParams.TOKEN) && !data.getStringExtra(PaymentParams.TOKEN).isEmpty()) {
                Log.e("Tag", data.getStringExtra(PaymentParams.TOKEN));
                Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_EMAIL));
                Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_PASSWORD));
                if (((Switch) findViewById(R.id.switch_tokenization)).isChecked()) {
                    token = data.getStringExtra(PaymentParams.TOKEN);
                    cmail = data.getStringExtra(PaymentParams.CUSTOMER_EMAIL);
                    cpwd = data.getStringExtra(PaymentParams.CUSTOMER_PASSWORD);
                }
            }
            Intent resultIntent = new Intent(getApplicationContext(), PaymentResultActivity.class);
            resultIntent.putExtra(PaymentParams.RESPONSE_CODE, data.getStringExtra(PaymentParams.RESPONSE_CODE));
            resultIntent.putExtra(PaymentParams.RESULT_MESSAGE, data.getStringExtra(PaymentParams.RESULT_MESSAGE));
            resultIntent.putExtra(PaymentParams.TRANSACTION_ID, data.getStringExtra(PaymentParams.TRANSACTION_ID));
            startActivityForResult(resultIntent, Constants.PAYMENT_RESULTS_CODE);

        } else if (requestCode == Constants.PAYMENT_SCAN_CODE) {
            ((TextView) findViewById(R.id.tv_card_name)).setText("");
            ((TextView) findViewById(R.id.tv_card_number)).setText("");
            ((TextView) findViewById(R.id.tv_card_month)).setText("");
            ((TextView) findViewById(R.id.tv_card_year)).setText("");
            paymentRequestModel.resetCard();
            if (resultCode == RESULT_OK) {
                Card card = data.getParcelableExtra(ScanCardIntent.RESULT_PAYCARDS_CARD);
                if (card == null) {
                    Toast.makeText(this, getString(R.string.error_scan), Toast.LENGTH_LONG).show();
                } else {
                    paymentRequestModel.setCardName(card.getCardHolderName());
                    paymentRequestModel.setCardNumber(card.getCardNumber());

                    if (card.getExpirationDate() != null && card.getExpirationDate().length() == 5) {
                        paymentRequestModel.setCardYear(card.getExpirationDate().substring(3));
                        paymentRequestModel.setCardMonth(card.getExpirationDate().substring(0, 2));
                    }

                    if (!TextUtils.isEmpty(paymentRequestModel.getCardName())) {
                        ((TextView) findViewById(R.id.tv_card_name)).setText(paymentRequestModel.getCardName());
                    }
                    if (!TextUtils.isEmpty(paymentRequestModel.getCardNumber())) {
                        ((TextView) findViewById(R.id.tv_card_number)).setText(paymentRequestModel.getCardNumber());
                    }
                    if (!TextUtils.isEmpty(paymentRequestModel.getCardMonth())) {
                        ((TextView) findViewById(R.id.tv_card_month)).setText(paymentRequestModel.getCardMonth());
                    } else {
                        ((TextView) findViewById(R.id.tv_card_month)).setText("");
                    }
                    if (!TextUtils.isEmpty(paymentRequestModel.getCardYear())) {
                        ((TextView) findViewById(R.id.tv_card_year)).setText(paymentRequestModel.getCardYear());
                    } else {
                        ((TextView) findViewById(R.id.tv_card_year)).setText("");
                    }
                }
            } else {
                Toast.makeText(this, getString(R.string.error_scan), Toast.LENGTH_LONG).show();
            }
        }
    }
}