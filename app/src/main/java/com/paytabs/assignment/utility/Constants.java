package com.paytabs.assignment.utility;

public class Constants {

    public static final int PAYMENT_SCAN_CODE = 2000;
    public static final int PAYMENT_RESULTS_CODE = 3000;

    public static final String SECRET_KEY = "eTPwDdtKKsZpBHsRPD7d9lEAD4L3wwlKqbg3oKN9cNyNu1EkEMyO7doKh9ONGax6lO9JzXTLKszfteA819W9mkTDtIraSYgdNqXm";
    public static final String MERCHANT_EMAIL = "mustafa.anan@hotmail.com";
    public static final String DEFAULT_POSTAL = "00973";

    public static final String SCAN_CARD_NAME = "scan_card_name";
    public static final String SCAN_CARD_NUMBER = "scan_card_number";
    public static final String SCAN_CARD_MONTH = "scan_card_month";
    public static final String SCAN_CARD_YEAR = "scan_card_year";

}
